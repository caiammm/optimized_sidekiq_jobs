FactoryBot.define do
  factory :student do
    name { Faker::Name.name }
    age { Random.rand(80) }
    address { Faker::Address.street_address }
    address_number { Faker::Address.building_number  }
    postal_code { Faker::Address.zip }
    neighborhood { Faker::Address.community }
    phone { Faker::PhoneNumber.cell_phone }
    course_id nil
  end
end
