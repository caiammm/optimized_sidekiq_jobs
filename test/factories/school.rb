FactoryBot.define do
  factory :school do
    name { Faker::Company.name }
    full_name { Faker::Company.name }
    about { Faker::Lorem.paragraph_by_chars(number: 256, supplemental: false) }
    field1 { Faker::Lorem.paragraph_by_chars(number: 256, supplemental: false) }
    field2 { Faker::Lorem.paragraph_by_chars(number: 256, supplemental: false) }
    some_json { Faker::Json.shallow_json(width: 3, options: { key: 'Address.community', value: 'Address.building_number' }) }
    another_json { Faker::Json.shallow_json(width: 3, options: { key: 'Address.community', value: 'Address.building_number' }) }
  end
end
