FactoryBot.define do
  factory :unity do
    name { Faker::Company.name }
    full_name { Faker::Company.name }
    city { Faker::Address.city  }
    state { Faker::Address.state  }
    address { Faker::Address.street_address }
    address_number { Faker::Address.building_number  }
    postal_code { Faker::Address.zip }
    neighborhood { Faker::Address.community }
  end
end
