FactoryBot.define do
  factory :course do
    name { Faker::Commerce.product_name }
    price { Faker::Commerce.price }
    shift { ['manhã', 'noite'][rand(2)] }
    unity_id nil
    about { Faker::Lorem.paragraph_by_chars(number: 256, supplemental: false) }
  end
end








