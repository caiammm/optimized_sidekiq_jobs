roadmap:

- prepare memory profiler and logs

- create in a bad architecture: workers, situation and big repetitions to prove the memory inflating

- create in a good architecture: workers, different code but same result wanted above

- post results showing difference that the architecture can impact in the memory from your server.

