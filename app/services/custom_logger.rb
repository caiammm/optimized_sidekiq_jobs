class CustomLogger

  def initialize(relative_path = "generic_log.log")
    @logger = Logger.new("#{Rails.root}/custom_log/" + relative_path)
  end

  def custom_log_message(message)
    @logger.info("\n#{message}")
  end
end
