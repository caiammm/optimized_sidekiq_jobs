class Review < ApplicationRecord
  belongs_to :school
  belongs_to :unity
  belongs_to :course
  belongs_to :student
end
