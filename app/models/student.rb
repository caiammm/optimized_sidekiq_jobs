class Student < ApplicationRecord
  belongs_to :course
  has_one :unity, through: :course
  has_one :school, through: :unity

end
