class ProcessingMethodA
  include Sidekiq::Worker

  def perform
    log = CustomLogger.new("processing_method_a")

    mem = GetProcessMem.new.mb.tap do |mb|
      students_ids = Student.all.pluck(:id);
      log.custom_log_message("STARTING memory: #{mb}")

      students_ids.each do |student_id|
        # student = Student.includes(:course, :unity, :school).find(student_id);
        student = Student.find(student_id);

        review = Review.new(
          note_record: "abs abs #{student_id}, abs abs #{student_id * 2}",
          student_id: student.id,
          course_id: student.course.id,
          unity_id: student.course.unity.id,
          school_id: student.course.unity.school.id
        )

        if review.valid?
          review.save
        end
        if review.persisted?
          puts Review.count
        end

        log.custom_log_message("memory: #{mb}")
      end;
      log.custom_log_message("FINISHING memory: #{mb}")
    end

  end
end
