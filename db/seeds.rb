# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

schools = FactoryBot.create_list(:school, 10)
schools.each do |school|
  FactoryBot.create_list(:unity, 10, school_id: school.id)
end

unities_ids = Unity.all.pluck(:id)
unities_ids.each do | unity_id |
  FactoryBot.create_list(:course, 10, unity_id: unity_id)
end;

courses_ids = Course.all.pluck(:id)

courses_ids.each do |course_id|
  FactoryBot.create_list(:student, 5, course_id: course_id)
end;

# students_ids = Student.all.pluck(:id);

# students_ids.each do |student_id|
#   student = Student.includes(:course, :unity, :school).find(student_id);

#   FactoryBot.create(
#     :review,
#     student_id: student.id,
#     course_id: student.course.id,
#     unity_id: student.course.unity.id,
#     school_id: student.course.unity.school.id
#     )
# end;
