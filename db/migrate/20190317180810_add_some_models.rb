class AddSomeModels < ActiveRecord::Migration[5.2]
  def change
    create_table :schools do |t|
      t.string :name
      t.string :full_name
      t.text :about
      t.text :field1
      t.text :field2
      t.jsonb :some_json
      t.jsonb :another_json
    end

    create_table :unities do |t|
      t.string :name
      t.string :full_name
      t.string :city
      t.string :state
      t.string :address
      t.string :address_number
      t.string :postal_code
      t.string :neighborhood
      t.integer :school_id
    end

    create_table :courses do |t|
      t.string :name
      t.float :price
      t.string :shift
      t.integer :unity_id
      t.text :about
    end

    create_table :students do |t|
      t.string :name
      t.integer :age
      t.string :address
      t.string :address_number
      t.string :postal_code
      t.string :neighborhood
      t.string :phone
      t.integer :course_id
    end

    create_table :reviews do |t|
      t.integer :school_id
      t.integer :unity_id
      t.integer :course_id
      t.integer :student_id
      t.string :note_record
    end

    add_index :unities, :school_id
    add_index :courses, :unity_id
    add_index :students, :course_id
    add_index :reviews, :school_id
    add_index :reviews, :unity_id
    add_index :reviews, :course_id
    add_index :reviews, :student_id

  end
end
