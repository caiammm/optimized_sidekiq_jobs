# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_17_180810) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.float "price"
    t.string "shift"
    t.integer "unity_id"
    t.text "about"
    t.index ["unity_id"], name: "index_courses_on_unity_id"
  end

  create_table "reviews", force: :cascade do |t|
    t.integer "school_id"
    t.integer "unity_id"
    t.integer "course_id"
    t.integer "student_id"
    t.string "note_record"
    t.index ["course_id"], name: "index_reviews_on_course_id"
    t.index ["school_id"], name: "index_reviews_on_school_id"
    t.index ["student_id"], name: "index_reviews_on_student_id"
    t.index ["unity_id"], name: "index_reviews_on_unity_id"
  end

  create_table "schools", force: :cascade do |t|
    t.string "name"
    t.string "full_name"
    t.text "about"
    t.text "field1"
    t.text "field2"
    t.jsonb "some_json"
    t.jsonb "another_json"
  end

  create_table "students", force: :cascade do |t|
    t.string "name"
    t.integer "age"
    t.string "address"
    t.string "address_number"
    t.string "postal_code"
    t.string "neighborhood"
    t.string "phone"
    t.integer "course_id"
    t.index ["course_id"], name: "index_students_on_course_id"
  end

  create_table "unities", force: :cascade do |t|
    t.string "name"
    t.string "full_name"
    t.string "city"
    t.string "state"
    t.string "address"
    t.string "address_number"
    t.string "postal_code"
    t.string "neighborhood"
    t.integer "school_id"
    t.index ["school_id"], name: "index_unities_on_school_id"
  end

end
